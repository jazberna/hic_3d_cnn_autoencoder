from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Reshape,Conv3D, Conv3DTranspose, Flatten, Conv2D, Conv2DTranspose, Input
import numpy as np
import glob
import os
import plotly.express as px
import pandas as pd
import sys
import tabix

def generate_autoencoder(number_of_bins,encoding_size):
    sample_shape=tuple([number_of_bins,number_of_bins,number_of_bins,1])
    autoencoder1 = Input(shape=sample_shape, name='encoder_input')
    autoencoder2 = Conv3D(1, kernel_size=(3, 3, 3), strides=(1,1,1), activation='relu', kernel_initializer='he_uniform', input_shape=sample_shape, padding='same')(autoencoder1)
    autoencoder3 = Flatten()(autoencoder2)
    autoencoder4 = Dense(encoding_size, activation='relu',  name='encoder_last')(autoencoder3)
    autoencoder5 = Dense(number_of_bins * number_of_bins * number_of_bins , activation='relu',  name='decoder_first')(autoencoder4)
    autoencoder6 = Reshape(sample_shape)(autoencoder5)
    autoencoder7 = Conv3DTranspose(1,(3,3,3), strides=(1,1,1),  padding='same', activation='sigmoid')(autoencoder6)
    autoencoder = Model(autoencoder1,autoencoder7)

    encoder = generate_encoder(autoencoder)
    decoder = generate_decoder(autoencoder,encoding_size)
    return autoencoder, encoder, decoder

def generate_encoder(autoencoder):
    encoder = Model(autoencoder.layers[0].output,autoencoder.layers[3].output)
    return encoder

def generate_decoder(autoencoder,encoding_size):
    decoder_layers = autoencoder.layers[-3:]
    input = Input(shape=(encoding_size,),name='decoder_input')
    output1=decoder_layers[0](input)
    output2=decoder_layers[1](output1)
    output3=decoder_layers[2](output2)
    decoder = Model(input,output3)
    return decoder

def create_dataset(base_dir,max_axis,chr,number_of_bins,coords_limits,chr_bins):

    gmin_x=coords_limits['gmin_x']
    gmin_y=coords_limits['gmin_y']
    gmin_z=coords_limits['gmin_z']
    gmax_x=coords_limits['gmax_x']
    gmax_y=coords_limits['gmax_y']
    gmax_z=coords_limits['gmax_z']

    cartesian_files = glob.glob(os.path.join(base_dir,'*parsed.xyz'))

    final_dataset_dims = tuple([len(cartesian_files)]) + (number_of_bins, number_of_bins, number_of_bins, 1)
    final_dataset = np.zeros(final_dataset_dims)
    final_dataset_chr_bins = list()
    for index,cartesian_file in enumerate(cartesian_files):
        array_from_file = np.loadtxt(cartesian_file, dtype='float32')
        extra = max_axis - array_from_file.shape[0]
        if extra != 0:
            array_from_file = np.vstack( (array_from_file , np.zeros((extra,3))))

        bins_x = np.linspace(gmin_x,gmax_x,number_of_bins -1 ).astype('float64')
        bins_y = np.linspace(gmin_y,gmax_y,number_of_bins -1 ).astype('float64')
        bins_z = np.linspace(gmin_z,gmax_z,number_of_bins -1 ).astype('float64')

        ret1=np.digitize(array_from_file[:,0],bins_x).reshape((max_axis,1))
        ret2=np.digitize(array_from_file[:,1],bins_y).reshape((max_axis,1))
        ret3=np.digitize(array_from_file[:,2],bins_z).reshape((max_axis,1))
        ret = np.hstack((ret1,ret2,ret3))
        volume=np.zeros((number_of_bins,number_of_bins,number_of_bins,1))
        this_volume_chr_bins = {}
        for start, row in enumerate(ret):
            end = start+1
            volume[row[0],row[1],row[2]] = 1
            x_y_z='_'.join(list(map(lambda x: str(x), row)))
            if end < len(chr_bins):
                this_volume_chr_bins[ x_y_z ] = "{}:{}-{}".format(chr,chr_bins[start],chr_bins[end])
        final_dataset_chr_bins.append(this_volume_chr_bins)
        final_dataset[index]=volume

    final_dataset = final_dataset.astype('int32')
    return final_dataset, final_dataset_chr_bins


def get_genes_in_bin(bed_file,coords_string):
    try:
        tb = tabix.open(bed_file)
        records = tb.querys(coords_string)
        return str(list(map(lambda record: record[3],records)))
    except:
        return ''

if __name__ == '__main__' :

    base_dir='/Users/jazberna/HIC_PDB/SUPERIMPOSE'
    chr=1
    chr_start=0
    chr_end=248000000
    max_axis=913
    coords_limits={
        'gmin_x':-6.379000,
        'gmin_y':-5.817144,
        'gmin_z':-7.443827,
        'gmax_x':6.772533,
        'gmax_y':6.849885,
        'gmax_z':7.155879
    }
    number_of_bins=60
    chr_bins = np.linspace(chr_start, chr_end, max_axis).astype('int32')

    dataset, dataset_chr_bins = create_dataset(base_dir,max_axis,chr,number_of_bins,coords_limits,chr_bins)
    encoding_size=30
    sample_shape=(number_of_bins,number_of_bins,number_of_bins,1)

    if sys.argv[1] == 'train':
        autoencoder,encoder,decoder = generate_autoencoder(number_of_bins,encoding_size)
        autoencoder.summary()
        encoder.summary()
        decoder.summary()

        autoencoder.compile(optimizer='adam', loss='mean_squared_error', metrics=['accuracy'])
        autoencoder.fit(dataset,dataset,batch_size=20,epochs=100)


        autoencoder.save('volume_autoencoder.h5')
        encoder.save('volume_encoder.h5')
        decoder.save('volume_decoder.h5')

    if sys.argv[1] == 'predict':

        autoencoder = load_model('volume_autoencoder.h5')
        encoder = load_model('volume_encoder.h5')
        decoder = load_model('volume_decoder.h5')

        embeddings1 = encoder.predict(dataset[0:1])
        embeddings2 = encoder.predict(dataset[19:20])
        difference_embedding = embeddings1[0] - embeddings2[0]
        difference_volume = decoder.predict(np.reshape(difference_embedding,(1,encoding_size,1)))[0]
        query1 = decoder.predict(np.reshape(embeddings1[0],(1,encoding_size,1)))[0]
        query2 = decoder.predict(np.reshape(embeddings2[0],(1,encoding_size,1)))[0]

        difference_volume_bins=np.empty(difference_volume.shape).astype('str')
        for x in range(0,number_of_bins):
            for y in range(0,number_of_bins):
                for z in range(0,number_of_bins):
                    if dataset[0:1][0][x,y,z] == 1 or dataset[19:20][0][x,y,z] == 1:
                        x_y_z = '_'.join([str(x),str(y),str(z)])
                        if x_y_z in dataset_chr_bins[19:20][0]:
                            difference_volume_bins[x,y,z]=dataset_chr_bins[19:20][0][x_y_z]
                        if x_y_z in dataset_chr_bins[0:1][0]:
                            difference_volume_bins[x,y,z]=dataset_chr_bins[0:1][0][x_y_z]
                        pass
                    else:
                        difference_volume[x,y,z]=0
                        query1[x,y,z]=0
                        query2[x,y,z]=0

        x_diff_vector = list()
        y_diff_vector = list()
        z_diff_vector = list()
        type_vector = list()
        diff_intensity=list()
        start_end_coords=list()
        genes = list()
        if sys.argv[2]=='chrs':
            for x in range(0,number_of_bins):
                for y in range(0,number_of_bins):
                    for z in range(0,number_of_bins):
                        if dataset[0:1][0][x,y,z] > 0:
                        #if query1[x,y,z] > 0:
                            x_diff_vector.append(x)
                            y_diff_vector.append(y)
                            z_diff_vector.append(z)
                            type_vector.append('chr1_time_a')

                        if dataset[19:20][0][x,y,z] > 0:
                        #if query2[x,y,z] > 0:
                            x_diff_vector.append(x)
                            y_diff_vector.append(y)
                            z_diff_vector.append(z)
                            type_vector.append('chr1_time_b')

            df = pd.DataFrame(list(zip(x_diff_vector, y_diff_vector, z_diff_vector, type_vector)),columns =['X', 'Y', 'Z','TYPE'])
            fig = px.scatter_3d(df, x='X', y='Y', z='Z',color='TYPE',opacity=0.5)
            fig.show()
        if sys.argv[2]=='difference':
            bed_file = sys.argv[3]
            for x in range(0,number_of_bins):
                for y in range(0,number_of_bins):
                    for z in range(0,number_of_bins):
                        if difference_volume[x,y,z][0] > 0:
                            x_diff_vector.append(x)
                            y_diff_vector.append(y)
                            z_diff_vector.append(z)
                            type_vector.append('difference')
                            diff_intensity.append(difference_volume[x,y,z][0])
                            start_end_coords.append(difference_volume_bins[x,y,z][0])
                            genes.append(get_genes_in_bin(bed_file,difference_volume_bins[x,y,z][0]))

            df = pd.DataFrame(list(zip(x_diff_vector, y_diff_vector, z_diff_vector, type_vector, diff_intensity,start_end_coords, genes)),columns =['X', 'Y', 'Z','TYPE','INTENSITY','COORDINATES','ONCOGENES'])
            df = df.drop_duplicates()
            fig = px.scatter_3d(df, x='X', y='Y', z='Z',color='INTENSITY',hover_data=['COORDINATES','ONCOGENES'],opacity=0.3, range_x=(0,number_of_bins), range_y=(0,number_of_bins), range_z=(0,number_of_bins))
            fig.show()

    else:
        print("options are {} {} {}".format('python',sys.argv[0],'train'))
        print("options are {} {} {} {}".format('python',sys.argv[0],'train','chrs'))
        print("options are {} {} {} {}".format('python',sys.argv[0],'train','difference','bed.gz'))
