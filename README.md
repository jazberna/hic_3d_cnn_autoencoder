# [HiC 3D Convolutional Neural Network Autoencoder](media/HiC_video_git.mov)
**A proof of concept using data from HiC experiments where a cell line is exposed to Dexamethasone, a drug used to treat COVID-19.**

The motivation for this project is being able to represent 3D HiC based chromosomomes as embeddings, subtract the embeddings for the same chromosome at different experimental conditions and express the extructural changes as the decoded 'subtracted' embedding.

#### The original data was published here:
https://doi.org/10.1016/j.cels.2018.06.007

D'Ippolito AM, McDowell IC, Barrera A, Hong LK, Leichter SM, Bartelt LC, Vockley CM, Majoros WH, Safi A, Song L, Gersbach CA, Crawford GE, Reddy TE. Pre-established Chromatin Interactions Mediate the Genomic Response to Glucocorticoids. Cell Syst. 2018 Aug 22;7(2):146-160.e7. doi: 10.1016/j.cels.2018.06.007. Epub 2018 Jul 18. PMID: 30031775.

#### I got the actual data below from:
http://sysbio.rnet.missouri.edu/3dgenome/GSDB/


## [Traning Dataset:](training_dataset)
The pdb files used to create the [training dataset](training_dataset) were obainted from [GSDB](http://sysbio.rnet.missouri.edu/3dgenome/GSDB/browse.php). Specfically I used these datasets 'Hi-C on A549 cell line treated with 100 nM dexamethasone for 0,1,4,8 and 12 hours' to get the chromosome 1 250kb resolution [SIMBA3D](https://github.com/nerettilab/SIMBA3D) pdb files. Those files were processed with [PyMOL](https://pymol.org/) to align and superpoimpose the chromosomes for which I finally only left the X,Y,Z coordinates.

## 3D CNN Autoencoder:
The 3D CNN Autoencoder Keras model I came up with is basically this:
```
def generate_autoencoder(number_of_bins,encoding_size):
    sample_shape=tuple([number_of_bins,number_of_bins,number_of_bins,1])
    autoencoder1 = Input(shape=sample_shape, name='encoder_input')
    autoencoder2 = Conv3D(1, kernel_size=(3, 3, 3), strides=(1,1,1), activation='relu', kernel_initializer='he_uniform', input_shape=sample_shape, padding='same')(autoencoder1)
    autoencoder3 = Flatten()(autoencoder2)
    autoencoder4 = Dense(encoding_size, activation='relu',  name='encoder_last')(autoencoder3)
    autoencoder5 = Dense(number_of_bins * number_of_bins * number_of_bins , activation='relu',  name='decoder_first')(autoencoder4)
    autoencoder6 = Reshape(sample_shape)(autoencoder5)
    autoencoder7 = Conv3DTranspose(1,(3,3,3), strides=(1,1,1),  padding='same', activation='sigmoid')(autoencoder6)
    autoencoder = Model(autoencoder1,autoencoder7)

    encoder = generate_encoder(autoencoder)
    decoder = generate_decoder(autoencoder,encoding_size)
    return autoencoder, encoder, decoder

def generate_encoder(autoencoder):
    encoder = Model(autoencoder.layers[0].output,autoencoder.layers[3].output)
    return encoder

def generate_decoder(autoencoder,encoding_size):
    decoder_layers = autoencoder.layers[-3:]
    input = Input(shape=(encoding_size,),name='decoder_input')
    output1=decoder_layers[0](input)
    output2=decoder_layers[1](output1)
    output3=decoder_layers[2](output2)
    decoder = Model(input,output3)
    return decoder
```
## Extra model information:
The model is compiled and fitted as:
```
        autoencoder.compile(optimizer='adam', loss='mean_squared_error', metrics=['accuracy'])
        autoencoder.fit(dataset,dataset,batch_size=20,epochs=100)
```

## The saved models here:
[encoder](https://www.dropbox.com/s/uuy7jw4dtzgwxmk/volume_encoder.h5?dl=0)

[decoder](https://www.dropbox.com/s/21fldacfej1j5in/volume_decoder.h5?dl=0)


## [Traning script](src/hic_3d_autoencoder2.py)
To use the [training script](src/hic_3d_autoencoder2.py) make sure to chage this varible in the code to point to the folder where the training dataset is located, for instance:
```
    base_dir='/Users/jazberna/HIC_PDB/SUPERIMPOSE'
```
To train the model run:
```
    python real_hic_3d_autoencoder.py train
```

To run the model and get the 3D plot of two chromosomes 1 at different times as the left side of the [video](media/hic_3d_autoencoder.mov) below:
```
    python real_hic_3d_autoencoder.py predict chrs
```

To run the model and get the 3D plot showing the strcutural differnces as color intensities of two chromosomes 1 at different times, as the right side of the [video](media/hic_3d_autoencoder.mov) below. 
```
    python real_hic_3d_autoencoder.py predict difference
```

![](media/hic_3d_autoencoder.mov)

![](media/annotations.png)